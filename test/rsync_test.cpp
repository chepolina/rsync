#include "rsync.h"
#include <gtest/gtest.h>
#include <future>

TEST(rsync, All) {
    system("rm -r a && rm -r b && mkdir a && mkdir b && touch a/file1 && touch a/file2 touch a/file4 && touch b/file2 && touch b/file3");
    auto handle_sender = std::async(std::launch::async,
                              sender_run, 65000, "a");
    auto handle_receiver = std::async(std::launch::async,
                              receiver_run, "127.0.0.1", 65000, "b");
    handle_sender.get();
    handle_receiver.get();
    testing::internal::CaptureStdout();
    system("ls a");
    std::string output_a = testing::internal::GetCapturedStdout();
    testing::internal::CaptureStdout();
    system("ls b");
    std::string output_b = testing::internal::GetCapturedStdout();
    ASSERT_EQ(output_a, output_b);
}