#pragma once

#include <vector>
#include <string>
#include <sys/socket.h>
#include <error.h>
#include <sys/types.h>
#include <netdb.h>
#include <strings.h>

#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

enum class MsgId : unsigned char {
    GETLIST = 1,
    FILELIST = 2,
    REMOVE = 3,
    FILE = 4,
    FILE_PART = 5,
    OK = 6,
    DONE = 7
};

struct Frame {
    MsgId msg_id;

    bool last;
    std::string body;
};

class Connection {
public:
    virtual ~Connection() {}
    virtual void WriteFrame(Frame* frame) = 0;
    virtual void ReadFrame(Frame* frame) = 0;
};

class LocalSocketConnection : public Connection {
public:
    LocalSocketConnection(int fd) : fd_(fd) {}
//    ~LocalSocketConnection();

    virtual void WriteFrame(Frame* frame);
    virtual void ReadFrame(Frame* frame);

private:
    int fd_;
};

class SocketConnection : public Connection {
public:
    SocketConnection(int port);
    SocketConnection(const char* host_name, int port);
    ~SocketConnection();

    virtual void WriteFrame(Frame* frame);
    virtual void ReadFrame(Frame* frame);

private:
    int sockfd;
    int oldsockfd;
};

struct FileList {
    std::vector<std::string> files = {};

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & files;
    }
};

struct FileInfo {
    std::string name;
    mode_t mode;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & name;
        ar & mode;
    }
};

class Protocol {
public:
    Protocol(Connection* conn, std::string dir_name) : conn_(conn), dir_name(dir_name) {}

    void SendGetList();
    void SendFileList(const FileList& list);
    bool SendRemove(const FileList& list);
    void SendOk();
    bool SendFiles(const FileList& list);
    void SendDone();

    MsgId RecvMsg(); // reads next frame
    FileList GetFileList();
    FileList GetRemove();
    FileInfo GetFileInfo();
    std::string GetFilePart(bool* last);

private:
    Connection* conn_;
    Frame last_received_;
    std::string dir_name;
};

void ReadDir(FileList& filelist, const char* dir_name);
void removeFiles(const FileList& list);
void sender_run(int port, const char* sender_dir);
void receiver_run(const char* host_name, int port, const char* receiver_dir);
void sync(const char* sender_dir, const char* receiver_dir);