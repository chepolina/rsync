#include "rsync.h"
#include <string.h>
#include <iostream>
#include <string>

void print_info() {
    std::cout << "./rsync sender port folder\n./rsync receiver address port folder" << std::endl;
}

int main(int argc, char* argv[]) {
    if (argc == 1) {
        print_info();
        exit(0);
    }
    if (strcmp(argv[1], "sender") == 0){
        sender_run(std::stoi(argv[2]), argv[3]);
    } else if (strcmp(argv[1], "receiver") == 0) {
        receiver_run(argv[2], std::stoi(argv[3]), argv[4]);
    } else {
        print_info();
        exit(0);
    }
    return 0;
}