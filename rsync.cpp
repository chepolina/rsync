#include "rsync.h"
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/socket.h>
#include <thread>
#include <functional>
#include <iostream>
#include <fcntl.h>

SocketConnection::SocketConnection(int port) {
    oldsockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (oldsockfd < 0) 
        error(0, 0, "ERROR opening socket");
    struct sockaddr_in serv_addr, cli_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(oldsockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
        error(0, 0, "ERROR on binding");
        std::cout << errno << std::endl;
    }
    listen(oldsockfd, 5);
    socklen_t clilen = sizeof(cli_addr);
    sockfd = accept(oldsockfd, 
                   (struct sockaddr *) &cli_addr, 
                    &clilen);
    if (sockfd < 0)
        error(0, 0, "ERROR on accept");
}
SocketConnection::SocketConnection(const char* host_name, int port) {
    oldsockfd = -1;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error(0, 0, "ERROR opening socket");
    struct hostent *server = gethostbyname(host_name);
    if (server == NULL) {
        error(0, 0, "ERROR, no such host");
        exit(0);
    }
    struct sockaddr_in serv_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(port);
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error(0, 0, "ERROR connecting");
}
SocketConnection::~SocketConnection() {
    close(sockfd);
    if (oldsockfd > 0)
        close(oldsockfd);
}

void SocketConnection::WriteFrame(Frame* frame) {
    uint32_t len = frame->body.size();
    write(sockfd, &len, sizeof(uint32_t));
    write(sockfd, &(frame->msg_id), 1);
    unsigned char flag[3] = {};
    flag[0] |= ((unsigned char)frame->last) << 0;
    write(sockfd, flag, 3);
    write(sockfd, frame->body.c_str(), len);
}

void SocketConnection::ReadFrame(Frame* frame) {
    uint32_t len = 0;
    read(sockfd, &len, 4);
    read(sockfd, &(frame->msg_id), 1);
    unsigned char flag[3] = {};
    read(sockfd, flag, 3);
    frame->last = (flag[0] >> 0) & 1;
    frame->body.resize(len);
    read(sockfd, &frame->body[0], len);
}

void Protocol::SendGetList() {
    Frame f;
    f.msg_id = MsgId::GETLIST;
    conn_->WriteFrame(&f);
}

void Protocol::SendFileList(const FileList& list) {
    Frame f;
    f.msg_id = MsgId::FILELIST;
    f.last = true;
    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << list;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

MsgId Protocol::RecvMsg() {
    conn_->ReadFrame(&last_received_);
    return last_received_.msg_id;
}

FileList Protocol::GetFileList() {
    std::stringstream ss;
    ss << last_received_.body;

    FileList list;
    boost::archive::text_iarchive archive(ss);
    archive >> list;

    return list;
}

bool Protocol::SendRemove(const FileList& list) {
    Frame f;
    f.msg_id = MsgId::REMOVE;
    f.last = true;
    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << list;
    f.body = ss.str();

    conn_->WriteFrame(&f);
}

void Protocol::SendOk() {
    Frame f;
    f.msg_id = MsgId::OK;
    f.last = true;
    f.body = "";
    conn_->WriteFrame(&f);
}

bool Protocol::SendFiles(const FileList& list) {
    for (int i = 0; i < list.files.size(); ++i) {
        Frame f;
        struct stat infoFile;
        stat((dir_name + "/" + list.files[i]).c_str(), &infoFile);
        FileInfo info;
        info.mode = infoFile.st_mode;
        info.name = list.files[i];
        f.msg_id = MsgId::FILE;
        f.last = true;
        std::stringstream ss;
        boost::archive::text_oarchive archive(ss);
        archive << info;
        f.body = ss.str();
        conn_->WriteFrame(&f);
        int fd = open((dir_name + "/" + list.files[i]).c_str(), O_RDONLY);
        const max_file_part = 4096;
        for (int i = 0; i <= infoFile.st_size; i += max_file_part) {
            Frame part;
            part.msg_id = MsgId::FILE_PART;
            part.last = (i + max_file_part > infoFile.st_size);
            if (part.last)
                part.body.resize(infoFile.st_size - i);
            else
                part.body.resize(max_file_part);
            read(fd, &part.body[0], part.body.size());
            conn_->WriteFrame(&part);
        }
        close(fd);
        if (RecvMsg() != MsgId::OK) {
            error(0, 0, "ERROR getting OK");
        }
    }
    return true;
}

void Protocol::SendDone(){
    Frame f;
    f.msg_id = MsgId::DONE;
    f.last = true;
    f.body = "";
    conn_->WriteFrame(&f);
}

FileList Protocol::GetRemove() {
    std::stringstream ss;
    ss << last_received_.body;

    FileList list;
    boost::archive::text_iarchive archive(ss);
    archive >> list;
    return list;
}

FileInfo Protocol::GetFileInfo() {
    std::stringstream ss;
    ss << last_received_.body;

    FileInfo info;
    boost::archive::text_iarchive archive(ss);
    archive >> info;
    return info;
}

std::string Protocol::GetFilePart(bool* last) {
    *last = last_received_.last;
    return last_received_.body;
}

void ReadDir(FileList& filelist, const char* dir_name) {
    filelist.files.clear();
    DIR* dir = opendir(dir_name);
    dirent* entry;
    while (entry = readdir(dir)) {
        filelist.files.push_back(entry->d_name);
    }
    std::sort(filelist.files.begin(), filelist.files.end());
}

void removeFiles(const FileList& list, std::string dir_name) {
    for (const std::string &name : list.files) {
        unlink((dir_name + "/" + name).c_str());
    }
}

void sender_run(int port, const char* sender_dir) {
    SocketConnection sender_conn(port);
    Protocol senderProt(&sender_conn, sender_dir);
    senderProt.SendGetList();
    senderProt.RecvMsg();
    FileList list_rec = senderProt.GetFileList();
    FileList list_sen;
    ReadDir(list_sen, sender_dir);
    FileList diff_rm;
    std::set_difference(list_rec.files.begin(), list_rec.files.end(),
                        list_sen.files.begin(), list_sen.files.end(),
                        std::back_inserter(diff_rm.files));
    senderProt.SendRemove(diff_rm);
    FileList diff_cp;
    std::set_difference(list_sen.files.begin(), list_sen.files.end(),
                        list_rec.files.begin(), list_rec.files.end(),
                        std::back_inserter(diff_cp.files));
    if (senderProt.SendFiles(diff_cp)) {
        senderProt.SendDone();
    } else {
        error(0, 0, "ERROR sending files");
    }
}

void receiver_run(const char* host_name, int port, const char* receiver_dir) {
    SocketConnection receiver_conn(host_name, port);
    Protocol receiverProt(&receiver_conn, receiver_dir);
    MsgId id;
    int fd = 0;
    id = receiverProt.RecvMsg();
    if (id == MsgId::GETLIST) {
        FileList filelist;
        ReadDir(filelist, receiver_dir);
        receiverProt.SendFileList(filelist);
    } else {
        error(0, 0, "ERROR getting file list");
    }

    id = receiverProt.RecvMsg();
    if (id == MsgId::REMOVE) {
        FileList container = receiverProt.GetRemove();
        removeFiles(container, receiver_dir);
    } else {
        error(0, 0, "ERROR getting remove list");
    }

    while (true) {
        id = receiverProt.RecvMsg();
        if (id == MsgId::FILE) {
            FileInfo info = receiverProt.GetFileInfo();
            fd = open((std::string(receiver_dir) + "/" + info.name).c_str(),
                      O_WRONLY | O_CREAT,
                      info.mode);
        } else if (id == MsgId::DONE) {
            break;
        } else {
            error(0, 0, "ERROR getting file info");
        }
        while (true) {
            id = receiverProt.RecvMsg();
            if (id == MsgId::FILE_PART) {
                bool last = false;
                std::string part = receiverProt.GetFilePart(&last);
                write(fd, part.c_str(), part.size());
                if (last) {
                    close(fd);
                    receiverProt.SendOk();
                    break;
                }
            }
        }
    }
}
